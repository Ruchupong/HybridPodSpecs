Pod::Spec.new do |spec|
    spec.name                   = "SpaceX"
    spec.version                = "1.0"
    spec.summary                = "SpaceX"
    spec.description            = "Hybrid component SpaceX"
    spec.homepage               = "https://gitlab.com/Ruchupong/HybridDistribute"
    spec.license                = { :type => 'MIT', :file => 'LICENSE' }
    spec.author                 = { "Ruchupong Saengan" => "Ruchupong_sae@truecorp.co.th" }
    spec.source                 = { :git => "git@gitlab.com:Ruchupong/HybridDistribute.git", :tag => 'spacex_1.0' }
    spec.vendored_frameworks    = "*.xcframework"
    spec.platform               = :ios
    spec.swift_version          = "4"
    spec.ios.deployment_target  = '11.0'
  end