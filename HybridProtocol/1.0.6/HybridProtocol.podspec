Pod::Spec.new do |spec|
    spec.name                   = "HybridProtocol"
    spec.version                = "1.0.6"
    spec.summary                = "HybridProtocol"
    spec.description            = "Hybrid component HybridProtocol"
    spec.homepage               = "https://gitlab.com/Ruchupong/HybridDistribute"
    spec.license                = { :type => 'MIT', :file => 'LICENSE' }
    spec.author                 = { "Ruchupong Saengan" => "Ruchupong_sae@truecorp.co.th" }
    spec.source                 = { :git => "git@gitlab.com:Ruchupong/HybridDistribute.git", :tag => 'hybridprotocol_1.0.6' }
    spec.vendored_frameworks    = "*.xcframework"
    spec.platform               = :ios
    spec.swift_version          = "4"
    spec.ios.deployment_target  = '11.0'
  end